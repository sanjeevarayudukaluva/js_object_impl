function mapObject(object, callback) {
  if (typeof object) {
    for (const key in object) {
      callback(key, object);
    }
    return object;
  } else {
    return null;
  }
}

module.exports = mapObject;
