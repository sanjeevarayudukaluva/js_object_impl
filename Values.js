function values(object) {
  if (typeof object) {
    const valuesArr = [];
    for (let key in object) {
      valuesArr.push(object[key]);
    }
    return valuesArr;
  } else {
    return null;
  }
}
module.exports = { values };
