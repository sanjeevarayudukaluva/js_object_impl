const testObject = require("../DataSet");
const mapObject = require("../MapObject");
function callback(value, Object) {
  return (Object[value] = "value of object");
}
try {
  const result = mapObject(testObject, callback);
  console.log(result);
} catch (error) {
  console.error(error);
}
