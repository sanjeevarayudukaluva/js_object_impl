function defaults(object, defaultProps) {
  if (typeof object && typeof defaultProps) {
    for (const key in defaultProps) {
      if (!Object.keys(object).includes(key)) {
        object[key] = defaultProps[key];
      }
    }
    return object;
  } else {
    return null;
  }
}

module.exports = defaults;
