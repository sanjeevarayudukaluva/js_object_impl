function invert(object) {
  if (typeof object) {
    let invertObject = {};
    for (const key in object) {
      value = object[key];
      invertObject[value] = key;
    }
    return invertObject;
  } else {
    return null;
  }
}

module.exports = invert;
