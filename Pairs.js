function pair(object) {
  if (typeof object) {
    const pairArr = [];
    for (let key in object) {
      pairArr.push(key, object[key]);
    }
    return pairArr;
  } else {
    return null;
  }
}
module.exports = pair;
