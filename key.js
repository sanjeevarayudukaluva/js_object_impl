function keys(object) {
  if (typeof object) {
    const keysArr = [];
    for (let key in object) {
      keysArr.push(key);
    }
    return keysArr;
  } else {
    return null;
  }
}
module.exports = keys;
